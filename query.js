const get_users = `
query {
    users_user{
        name
        id
        mobile
        email
        activities{
            event
        }
    }
}
`
module.exports = get_users;
const { gql } = require('apollo-server');
module.exports = gql`
type User{
    id:ID!
    name: String!
    email: String!
    mobile: String!
    activities: [Activity!]
}

type Activity {
    id: ID!
    event: String!
    description: String
}

type Query{
    get_users: [User]
    get_user_by_id(id:ID!): User
    get_activity_by_user_id(id:ID!): [Activity]
    get_activity_by_id(id:ID!): Activity
}

type Mutation{
    insert_user(name: String!, mobile:String!, email: String!):User
    insert_user_activity(user_id:ID!, event:String!, description:String): Activity
}
`
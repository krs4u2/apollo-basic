const { createApolloFetch } = require('apollo-fetch');
const get_users = require('./query');
const fetch = createApolloFetch({
    uri: 'https://todo-krs.herokuapp.com/v1/graphql',
});

module.exports = {
    Query: {
        get_users: () => {
            return fetch({
                query: get_users
            }).then(res => {
                console.log(res.data);
                return (res.data.users_user);
            })
        },
        get_user_by_id: (_, args) => {

        },
        get_activity_by_user_id: (_, args) => {

        },
        get_activity_by_id: (_, args) => {

        },
    },
    Mutation: {
        insert_user: (_, args) => {

        },
        insert_user_activity: (_, args) => {

        },
    }

}